import $ from 'jquery';

$('.motion-banner_btn.btn_1').click(function() {
    $('.motion-banner__info.full').toggle();
});

$('.motion-banner_btn.btn_2').click(function() {
    $('.motion-banner__info.back').toggle();
});

$('.motion-banner_btn.btn_3').click(function() {
    $('.motion-banner__info.front').toggle();
});
